import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HackerNews } from '../models/hacker-news';

@Injectable({
  providedIn: 'root'
})
export class HackerNewsService {

  selectHackerNews: HackerNews;
  hackerNews: HackerNews[];
  readonly URL_API = 'http://localhost:3000/api/news';

  constructor(private http: HttpClient) { 
    this.selectHackerNews = new HackerNews();
  }

  getHackerNews(){
    return this.http.get(this.URL_API);
  }

  putHackerNews(hn: HackerNews){
    return this.http.put(this.URL_API + `/${hn._id}`,hn);
  }

}
