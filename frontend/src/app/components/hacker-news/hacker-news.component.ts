import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HackerNewsService } from "../../services/hacker-news.service";
import { HackerNews } from '../../models/hacker-news';

@Component({
  selector: 'app-hacker-news',
  templateUrl: './hacker-news.component.html',
  styleUrls: ['./hacker-news.component.css'],
  providers: [HackerNewsService],
})
export class HackerNewsComponent implements OnInit {

  constructor(private hackerNewsService: HackerNewsService) { }

  ngOnInit() {
    this.getHackerNews();
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.hackerNewsService.selectHackerNews = new HackerNews();
    }
  }

  getHackerNews() {
    this.hackerNewsService.getHackerNews()
      .subscribe(res => {
        this.hackerNewsService.hackerNews = res as HackerNews[];
        console.log(res);
      });
  }

  deleteNews(hackerNews: HackerNews, form: NgForm) {
    if(confirm('Are you sure do you want to delete it?')) {
      this.hackerNewsService.putHackerNews(hackerNews)
        .subscribe(res => {
          this.resetForm(form);
          this.getHackerNews();
        });
    }
  }

  selectClickedRow(url: string){
    window.open(url, "_blank");
  }
  
}
