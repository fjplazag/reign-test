export class HackerNews {

    constructor(_id= '', objectID = '', title= '', author = '', url = '', created_at = new Date, story_state = 0){
        this._id = _id;
        this.objectID = objectID;
        this.title = title;
        this.author = author;
        this.url = url;
        this.created_at = created_at;
        this.story_state = story_state;
    }
    
    _id: string;
    objectID: string;
    title: string;
    author: string;
    url: string;
    created_at: Date;
    story_state: number;
}
