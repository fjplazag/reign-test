const express = require('express');
const router  =  express.Router();

const news = require('../controllers/hackerNews.controller');

router.get('/', news.getHackerNews);
router.post('/', news.createHackerNews);
router.put('/:id', news.editHackerNews);


module.exports = router;