const HackNews = require('../models/hackerNews');
const newsCtrl = {};
const fetch = require('node-fetch');


newsCtrl.getHackerNews = async (req, res) => {
    const hn = await HackNews.find({
        "story_state": 0
    }).sort({
        "created_at": -1
    });
    res.json(hn);
}


newsCtrl.createHackerNews = async (req, res) => {
    
    const API_URL = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";
    const response = await fetch(API_URL);
    const data = await response.json();
    
    let state = true;

    //for the control of duplicate and/or blocked news
    const DB_DATA = "http://localhost:3000/api/news"
    const db_response = await fetch(DB_DATA);
    const db_data = await db_response.json();


    for (var i = 0; i < data.hits.length; i++){
        
        const hn = new HackNews(data.hits[i]);

        for (var y = 0; y < db_data.length; y++){

            if (db_data[y].objectID === data.hits[i].objectID){
                // console.log("duplicado : "+ db_data[y].objectID + " - " + data.hits[i].objectID)
                state = false;
                break;
            }
        }
           
        //news without a title
        if (data.hits[i].story_title === null && data.hits[i].title === null){
            // console.log("descartado: " + data.hits[i].objectID + data.hits[i].story_title +  data.hits[i].title)
            state = false;
        } else if (data.hits[i].story_title === null){
            hn.title = data.hits[i].title;
        } else {
            hn.title = data.hits[i].story_title;
        }

        //check url content
        if (data.hits[i].url === null && data.hits[i].story_url === null){
            // console.log("descartado: " + data.hits[i].objectID + data.hits[i].story_url +  data.hits[i].url)
            state = false;
        } else if (data.hits[i].url === null){
            hn.url = data.hits[i].story_url;
        } else {
            hn.url = data.hits[i].url;
        }

        if (data.hits[i].author === null){
            hn.author = "";
        }  
        
        if (state){
            // console.log(hn);
            await hn.save();
        }
    }

    res.json({'status':'News Saved'});
}

newsCtrl.editHackerNews = async (req, res) => {
    const { id } = req.params;
    const hn = {
        story_state: 1//req.body.story_state
    };
    await HackNews.findByIdAndUpdate(id, {$set: hn});
    res.json({'status':'News Updated'});
}

module.exports = newsCtrl;