const mongoose = require('mongoose');
const { Schema } = mongoose; 

const HackerNewsSchema = new Schema({  
    objectID:{ type: String, required: true, unique : true},
    // story_title:{ type: String, required: false},
    title:{ type: String, required: true},
    author:{ type: String, required: true},
    // story_url:{ type: String, required: false},
    url:{ type: String, required: true},
    created_at:{ type: Date, default: Date.now, required: true},
    story_state: { type: Number, default: 0, required: true}
});

module.exports = mongoose.model('HackerNews', HackerNewsSchema);