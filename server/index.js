const express = require('express');
const morgan = require('morgan');
const app = express();
const {mongoose} = require('./database');
const HackNews = require('./models/hackerNews');
const fetch = require('node-fetch');
const cors = require('cors');

// Settings
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(cors({origin: 'http://localhost:4200'}));

async function loadNews(){

    const API_URL = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";
    const response = await fetch(API_URL);
    const data = await response.json();
    
    let state = true;

    //for the control of duplicate and/or blocked news
    const DB_DATA = "http://localhost:3000/api/news"
    const db_response = await fetch(DB_DATA);
    const db_data = await db_response.json();

    
    for (var i = 0; i < data.hits.length; i++){
        
        const hn = new HackNews(data.hits[i]);

        for (var y = 0; y < db_data.length; y++){

            if (db_data[y].objectID === data.hits[i].objectID){
                state = false;
                break;
            }
        }
           
        //news without a title
        if (data.hits[i].story_title === null && data.hits[i].title === null){
            state = false;
        } else if (data.hits[i].story_title === null){
            hn.title = data.hits[i].title;
        } else {
            hn.title = data.hits[i].story_title;
        }

        //check url content
        if (data.hits[i].url === null && data.hits[i].story_url === null){
            state = false;
        } else if (data.hits[i].url === null){
            hn.url = data.hits[i].story_url;
        } else {
            hn.url = data.hits[i].url;
        }

        if (data.hits[i].author === null){
            hn.author = "";
        }  
        
        if (state){
            await hn.save();
        }
    }
}


// Routes
app.use('/api/news',require('./routes/hackerNews.routes'));


// Starting the server
app.listen(app.get('port'), () =>{
    console.log('Server on port', app.get('port'));
    // loadNews();
    setInterval(() => {
        loadNews();
        console.log("New news about node.js added")
    }, 60 * 60 * 1000);
});