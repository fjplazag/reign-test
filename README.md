# Prueba Reign

Hola **Equipo Reign!**. El presente documento tiene como propósito dar a conocer, cuales son los pasos necesarios a efectuar, para que puedan revisar mi prueba. Gracias por su tiempo.
  

# Instalaciones
Luego de descargar el proyecto. Es necesario instalar las dependencias:
> **Para el servidor (/):** 
> npm install
>
> **Para el cliente (cd frontend/):** 
> npm install

# Arrancar App
Comprobar que se encuentra iniciado MongoDB en la maquina que ejecutaremos el proyecto y luego iniciar desde la terminal, el back y front de la aplicación.
> **Tener iniciado MongoDB:** 
>mongo
>
> **Para el servidor (/):** 
> npm run dev
> 
> **Para el cliente (cd frontend/):** 
> ng serve


# Utilizar App

Lo primero que debemos hacer para tener contenido en nuestra app y crear la base de datos en MongoDB, es utilizar una herramienta como **Postman**, para poder consumir la API de noticias: **[https://hn.algolia.com/api/v1/search_by_date?query=nodejs](https://hn.algolia.com/api/v1/search_by_date?query=nodejs)**

> Desde Postman:
>  
> 1. Hacer un petición **POST** a la dirección [http://localhost:3000/api/news](http://localhost:3000/api/news)
> 2. Obteniendo como respuesta:
> {"status": "News Saved"}
> 3. Ir al cliente: [http://localhost:4200/](http://localhost:4200/) y verificar que las noticias se encuentras cargadas, ordenas y guardadas en MongoDB.